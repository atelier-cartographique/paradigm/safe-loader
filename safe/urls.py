# urls.py
from django.urls import path
from .views import (
    delete_annotation,
    get_all_annotation_type_categories,
    get_all_annotation_types,
    get_all_cases,
    get_annotations_for,
    post_case,
    post_annotation,
)


def prefix(s, sep="/"):
    return f"safe{sep}{s}"


urlpatterns = [
    path(prefix("case/new"), post_case, name=prefix("post_case", "-")),
    path(
        prefix("annotation/post"), post_annotation, name=prefix("post_annotation", "-")
    ),
    path(
        prefix("annotation/delete/<annotation_id>"), delete_annotation, name=prefix("delete_annotation", "-")
    ),
    path(prefix("cases"), get_all_cases, name=prefix("get_all_cases", "-")),
    path(
        prefix("annotation/types"),
        get_all_annotation_types,
        name=prefix("get_all_annotation_types", "-"),
    ),
    path(
        prefix("annotation/type-categories"),
        get_all_annotation_type_categories,
        name=prefix("get_all_annotation_type_categories", "-"),
    ),
    path(
        prefix("annotation/for/<case_id>"),
        get_annotations_for,
        name=prefix("get_annotations_for", "-"),
    ),
]
