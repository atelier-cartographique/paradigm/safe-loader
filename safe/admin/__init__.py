from django.contrib import admin
from django.contrib.gis.admin.options import OSMGeoAdmin
from django.contrib.gis.forms import OSMWidget
from safe.models import (
    AnnotationTypeCategory,
    Case,
    Annotation,
    AnnotationGeometry,
    AnnotationType,
)


@admin.register(Case)
class CaseAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "source", "start_date", "end_date", "case_type")
    search_fields = ["name"]
    list_filter = ["case_type"]


@admin.register(Annotation)
class AnnotationAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "source",
        "start_date",
        "end_date",
        "acknowledgement_date",
        "type",
    )
    search_fields = ["start_date"]
    list_filter = ["type"]


@admin.register(AnnotationTypeCategory)
class AnnotationTypeCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(AnnotationType)
class AnnotationTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(AnnotationGeometry)
class AnnotationGeometryAdmin(OSMGeoAdmin):
    default_lon = 148655.42408386833
    default_lat = 170877.13142309058
    default_zoom = 8
    map_width = 600
    map_height = 600
