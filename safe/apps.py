from django.apps import AppConfig


class safeConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "safe"
    geodata = True
