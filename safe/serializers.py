from datetime import datetime
import json
from django.contrib.gis.geos.geometry import GEOSGeometry
from django.contrib.gis.geos import Point, LineString, Polygon
from api.models import UserMap
from .models import (
    AnnotationGeometry,
    AnnotationType,
    AnnotationTypeCategory,
    Case,
    Annotation,
    KeyWord,
)


def serialize_nullable(ser):
    def inner(value):
        if value is None:
            return None
        return ser(value)

    return inner


def serialize_datetime(dt: datetime):
    return int(dt.timestamp() * 1000)


def deserialize_datetime(ts):
    return datetime.fromtimestamp(ts / 1000)


def serialize_lingua(r):
    return r.to_dict()


def geojson(g: GEOSGeometry):
    return json.loads(g.geojson)


nullable_datetime = serialize_nullable(serialize_datetime)

nullable_lingua = serialize_nullable(serialize_lingua)

nullable_geojson = serialize_nullable(geojson)


def serialize_case(case: Case):
    return {
        "id": case.id,
        "case_type": case.case_type,
        "name": nullable_lingua(case.name),
        "source": nullable_lingua(case.source),
        "created_at": serialize_datetime(case.created_at),
        "end_date": nullable_datetime(case.end_date),
        "start_date": nullable_datetime(case.start_date),
        "informations": case.informations,
        "description": nullable_lingua(case.description),
        "mapId": case.base_map.id,
        "user": case.user.id,
    }


def deserialize_case(data):
    base_map = UserMap.objects.get(pk=data["mapId"])
    case = Case()
    case.informations = ""  # FIXME
    case.base_map = base_map
    for key in ["name", "source", "description", "case_type"]:
        setattr(case, key, data.get(key))

    case.created_at = deserialize_datetime(data["created_at"])
    if "end_date" in data and data["end_date"] is not None:
        case.end_date = deserialize_datetime(data["end_date"])
    if "start_date" in data and data["start_date"] is not None:
        case.start_date = deserialize_datetime(data["start_date"])

    return case


def serialize_annotation_type_category(cat: AnnotationTypeCategory):
    return {
        "id": cat.id,
        "name": serialize_lingua(cat.name),
        "description": serialize_lingua(cat.description),
    }


def serialize_annotation_type(at: AnnotationType):
    return {
        "id": at.id,
        "tag": at.tag,
        "name": at.name.to_dict(),
        "description": at.description.to_dict(),
        "codepoint": at.codepoint,
        "keywords": [serialize_lingua(kw) for kw in at.keyword_set.all()],
        "category": at.category.id,
    }


def serialize_annotation_geometry(ag: AnnotationGeometry):
    if ag.type == AnnotationGeometry.POINT:
        geom = ag.point
    elif ag.type == AnnotationGeometry.LINESTRING:
        geom = ag.line
    elif ag.type == AnnotationGeometry.POLYGON:
        geom = ag.polygon

    return {
        "id": ag.id,
        "type": "Feature",
        "geometry": geojson(geom),
        "properties": ag.properties,
    }


def serialize_annotation(a: Annotation):
    return {
        "id": a.id,
        "case": a.case.id,
        "type": a.type.id,
        "created": serialize_datetime(a.created_at),
        "start": nullable_datetime(a.start_date),
        "end": nullable_datetime(a.end_date),
        "acknowledgement": nullable_datetime(a.acknowledgement_date),
        "information": a.information_annex,
        "source": nullable_lingua(a.source),
        "user": a.user.id,
        "geometries": [
            serialize_annotation_geometry(ag) for ag in a.annotationgeometry_set.all()
        ],
    }


def deserialize_annotation(data):
    if (
        data["id"] == ""
    ):  # TODO: for creating a new annotation, because the front puts an empty string in that case
        annotation = Annotation()
    else:
        instance = Annotation.objects.get(pk=data["id"])
        if instance:
            annotation = instance
        else:
            annotation = Annotation()

    annotation.type = AnnotationType.objects.get(pk=data["type"])
    annotation.case = Case.objects.get(pk=data["case"])

    if "source" in data:
        annotation.source = data.get("source")

    if "information" in data:
        annotation.information_annex = data.get("information")

    annotation.created_at = deserialize_datetime(data["created"])
    if "end" in data and data["end"] is not None:
        annotation.end_date = deserialize_datetime(data["end"])
    if "start" in data and data["start"] is not None:
        annotation.start_date = deserialize_datetime(data["start"])
    if "acknowledgement" in data and data["acknowledgement"] is not None:
        annotation.acknowledgement_date = deserialize_datetime(data["acknowledgement"])

    return annotation


def get_geometry_type_from_tag(tag):
    if tag == "marker":
        return "Point"
    elif tag == "zone" or tag == "cone" or tag == "circle":
        return "Polygon"
    elif tag == "path":
        return "LineString"
    else:
        return None


def handle_geometries(data, annotation):
    if "geometries" in data:
        geometries = data["geometries"]
        if geometries:
            if (data["id"] == ""):  # creation
                annotation_geom = AnnotationGeometry()
            else:  # edit
                annotation_geom = AnnotationGeometry.objects.get(
                    annotation=data["id"])

            geometry_type = get_geometry_type_from_tag(annotation.type.tag)
            annotation_geom.annotation = annotation
            annotation_geom.type = geometry_type
            annotation_geom.properties = geometries[0]["properties"]
            if geometry_type == "Point":
                annotation_geom.point = Point(
                    geometries[0]["geometry"]["coordinates"])
            elif geometry_type == "LineString":
                annotation_geom.line = LineString(
                    geometries[0]["geometry"]["coordinates"])
            elif geometry_type == "Polygon":
                annotation_geom.polygon = Polygon(((geometries[0]["geometry"]["coordinates"][0])))
            else:
                pass  # TODO

            annotation_geom.save()

