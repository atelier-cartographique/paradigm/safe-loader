To allow the migration of a geometry into the gis schema (another schema than the default public) it was necessary to upgrade to the latest version of Django where an important bugfix was done to resolve this problem within Django.

For Cartostation this meant we had to resolve certain deprecations both in the carto-station app and the postgis-loader app.

Deprecations were the following:

- `force_text()` had to be replaced by `force_str()`
- `ugettext_lazy()` had to be replaced by `gettext_lazy()`
- `url()` had to be replaced by `re_path()`or `path()` (`re_path` allows for regex and is therefore preferable)

The necessary changes were made and commited within each submodule.

- carto-station : commit `update to django 4.2.4 ....` on branch `safe`
- postgis-loader : commit `update to django 4.2.4 ....` on branch `update_django` (**note** : for now this hasn't been pushed online yet. No access to repo)
- safe-loader : commit `update to django 4.2.4 ....` on branch `main`

Within the safe-docker root project the new commit references for these submodules have been commited with the exception of the postgis-loader submodule since the necessary commit has not been push online yet.

To remember: If in the future these changes are to be cherry-picked an integrated in the master branch of cartostation: verify that the file `/sdi/main/urls.py` does not contain the lines referring to "safe" urls (specific to safe project)