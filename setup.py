from setuptools import setup, find_packages

version = "1.0.0"

name = "safe"
description = "load django app for safeBxl"
url = "https://atelier-cartographique/paradigm/safe-loader.git"
author = "Champs Libres"
author_email = "info@champs-libres.coop"
license = "Affero GPL3"

classifiers = [
    "Development Status :: 1 - Alpha",
    "Intended Audience :: Safe Bruxelles",
    "Topic :: Software Development :: Application",
    "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
    "Operating System :: POSIX",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3.5",
]

install_requires = ["django"]

packages = find_packages()

setup(
    name=name,
    version=version,
    url=url,
    license=license,
    description=description,
    author=author,
    author_email=author_email,
    packages=packages,
    install_requires=install_requires,
    classifiers=classifiers,
    include_package_data=True,
)
